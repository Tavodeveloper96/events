<div>

    @if (session('user'))
        <div class="my-4 p-4">
            <div class="bg-green-100 p-4 rounded mb-4">
                <p class="text-green-700">Welcome, {{ session('user')->name }}! You are successfully logged in.</p>
            </div>
        </div>
    @endif
    @if (Auth::check())
    <div class="my-2 p-4">
        <div class="">
            Hola, <span class="text-xl">{{ Auth::user()->name }}</span>. Selecciona los eventos que quieres participar.
        </div>
    </div>
    @endif
    <div x-data="{
        events: [],
        fetchEvents() {
            fetch('/api/events')
                .then(response => response.json())
                .then(data => {
                    this.events = data;
                });
        },
        redirectToEvent(id) {
            window.location.href = `/event/${id}`;
        }
    }" x-init="fetchEvents()" class="container mx-auto p-4">
        <h1 class="text-2xl font-bold mb-4 flex items-center">
            <svg xmlns="http://www.w3.org/2000/svg" width="2em" height="2em" viewBox="0 0 64 64"><path fill="#cfd8dc" d="M5 38V14h38v24c0 2.2-1.8 4-4 4H9c-2.2 0-4-1.8-4-4"/><path fill="#10b981" d="M43 10v6H5v-6c0-2.2 1.8-4 4-4h30c2.2 0 4 1.8 4 4"/><g fill="#10b981"><circle cx="33" cy="10" r="3"/><circle cx="15" cy="10" r="3"/></g><path fill="#b0bec5" d="M33 3c-1.1 0-2 .9-2 2v5c0 1.1.9 2 2 2s2-.9 2-2V5c0-1.1-.9-2-2-2M15 3c-1.1 0-2 .9-2 2v5c0 1.1.9 2 2 2s2-.9 2-2V5c0-1.1-.9-2-2-2"/><path fill="#90a4ae" d="M13 20h4v4h-4zm6 0h4v4h-4zm6 0h4v4h-4zm6 0h4v4h-4zm-18 6h4v4h-4zm6 0h4v4h-4zm6 0h4v4h-4zm6 0h4v4h-4zm-18 6h4v4h-4zm6 0h4v4h-4zm6 0h4v4h-4zm6 0h4v4h-4z"/></svg>
            <span>Upcoming Events</span>
        </h1>
        <div class="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 gap-4">
            <template x-for="event in events" :key="event.id">
                <div class="bg-white p-4 rounded-lg shadow-md transform transition duration-500 hover:scale-105">
                    <h2 class="text-xl font-semibold mb-2" x-text="event.name"></h2>
                    <p class="text-gray-700" x-text="event.description"></p>
                    <p class="text-gray-600 flex gap-2 items-center"><svg xmlns="http://www.w3.org/2000/svg"
                            width="1em" height="1em" viewBox="0 0 24 24">
                            <path fill="currentColor"
                                d="M12 20a8 8 0 0 0 8-8a8 8 0 0 0-8-8a8 8 0 0 0-8 8a8 8 0 0 0 8 8m0-18a10 10 0 0 1 10 10a10 10 0 0 1-10 10C6.47 22 2 17.5 2 12A10 10 0 0 1 12 2m.5 5v5.25l4.5 2.67l-.75 1.23L11 13V7z" />
                        </svg> Date: <span x-text="event.date"></span></p>
                    <p class="text-gray-600 flex gap-2 items-center"><svg xmlns="http://www.w3.org/2000/svg"
                            width="1em" height="1em" viewBox="0 0 48 48">
                            <path fill="currentColor"
                                d="M24.005 15.5a6 6 0 1 0 0 12a6 6 0 0 0 0-12m-3.5 6a3.5 3.5 0 1 1 7 0a3.5 3.5 0 0 1-7 0M37 32L26.912 42.71a4 4 0 0 1-5.824 0L11 32h.038l-.017-.02l-.021-.025A16.92 16.92 0 0 1 7 21c0-9.389 7.611-17 17-17s17 7.611 17 17a16.92 16.92 0 0 1-4 10.955l-.021.025l-.017.02zm-1.943-1.619A14.43 14.43 0 0 0 38.5 21c0-8.008-6.492-14.5-14.5-14.5S9.5 12.992 9.5 21a14.43 14.43 0 0 0 3.443 9.381l.308.363l9.657 10.251a1.5 1.5 0 0 0 2.184 0l9.657-10.251z" />
                        </svg> Location: <span x-text="event.location"></span></p>
                    <p class="text-gray-600 flex gap-2 items-center"><svg xmlns="http://www.w3.org/2000/svg"
                            width="1em" height="1em" viewBox="0 0 1024 1024">
                            <path fill="currentColor"
                                d="M512 512a192 192 0 1 0 0-384a192 192 0 0 0 0 384m0 64a256 256 0 1 1 0-512a256 256 0 0 1 0 512m320 320v-96a96 96 0 0 0-96-96H288a96 96 0 0 0-96 96v96a32 32 0 1 1-64 0v-96a160 160 0 0 1 160-160h448a160 160 0 0 1 160 160v96a32 32 0 1 1-64 0" />
                        </svg> Capacity: <span x-text="event.capacity"></span></p>
                    <div class="mt-4">
                        <button wire:click="redirectToEvent(event.id)"
                            class="bg-blue-500 text-white px-4 py-2 rounded-full mr-2">Ver más</button>
                        <button wire:click="attendTheEvent(event.id)" class="bg-green-500 text-white px-4 py-2 rounded-full">Asistir al evento</button>
                    </div>
                </div>
            </template>
        </div>
    </div>

</div>
