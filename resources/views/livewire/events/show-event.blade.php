<div class="py-12">
    <div class="container m-auto px-4">
        <div class="px-4 mb-2">
            <a href="{{ url('/') }}" class="flex items-center gap-1">
                <svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" viewBox="0 0 32 32"><path fill="currentColor" d="M16.612 2.214a1.01 1.01 0 0 0-1.242 0L1 13.419l1.243 1.572L4 13.621V26a2.004 2.004 0 0 0 2 2h20a2.004 2.004 0 0 0 2-2V13.63L29.757 15L31 13.428ZM18 26h-4v-8h4Zm2 0v-8a2 2 0 0 0-2-2h-4a2 2 0 0 0-2 2v8H6V12.062l10-7.79l10 7.8V26Z"/></svg>
                <span>Home</span>
            </a>
        </div>
        @if (Auth::check())
        <div class="mb-4 p-4">
            @if($quantityRegistered)
                <p class="flex items-center"><svg xmlns="http://www.w3.org/2000/svg" width="1.5em" height="1.5em" viewBox="0 0 512 512"><path fill="none" stroke="currentColor" stroke-miterlimit="10" stroke-width="32" d="M366.05 146a46.7 46.7 0 0 1-2.42-63.42a3.87 3.87 0 0 0-.22-5.26l-44.13-44.18a3.89 3.89 0 0 0-5.5 0l-70.34 70.34a23.6 23.6 0 0 0-5.71 9.24a23.66 23.66 0 0 1-14.95 15a23.7 23.7 0 0 0-9.25 5.71L33.14 313.78a3.89 3.89 0 0 0 0 5.5l44.13 44.13a3.87 3.87 0 0 0 5.26.22a46.69 46.69 0 0 1 65.84 65.84a3.87 3.87 0 0 0 .22 5.26l44.13 44.13a3.89 3.89 0 0 0 5.5 0l180.4-180.39a23.7 23.7 0 0 0 5.71-9.25a23.66 23.66 0 0 1 14.95-15a23.6 23.6 0 0 0 9.24-5.71l70.34-70.34a3.89 3.89 0 0 0 0-5.5l-44.13-44.13a3.87 3.87 0 0 0-5.26-.22a46.7 46.7 0 0 1-63.42-2.32Z"/><path fill="none" stroke="currentColor" stroke-linecap="round" stroke-miterlimit="10" stroke-width="32" d="m250.5 140.44l-16.51-16.51m60.53 60.53l-11.01-11m55.03 55.03l-11-11.01m60.53 60.53l-16.51-16.51"/></svg> <span class="ml-1">Ya tienes {{ $quantityRegistered }} entradas para este evento.</span></p>
            @else
                <div class="">
                    <span class="text-xl font-semibold">{{ Auth::user()->name }}</span>, esperamos que puedas participar en nuestro evento.
                </div>
            @endif
        </div>
        @endif
        <div class="md:flex flex-row md:gap-6 justify-center lg:items-center">
            <div class="md:5/12 lg:w-1/2">
                <img src="{{URL::asset('/images/andicom.webp')}}" alt="andicom" border="0">
            </div>
            <div class="md:7/12 lg:w-1/2">
                <div class="p-4">
                    <h1 class="text-4xl mb-4 font-bold">{{ $event->name }}</h1>
                    <p class="text-2xl text-gray-700">{{ $event->description }}</p>
                    <p class="text-xl text-gray-600 flex gap-2 items-center"> <svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" viewBox="0 0 24 24"><path fill="currentColor" d="M12 20a8 8 0 0 0 8-8a8 8 0 0 0-8-8a8 8 0 0 0-8 8a8 8 0 0 0 8 8m0-18a10 10 0 0 1 10 10a10 10 0 0 1-10 10C6.47 22 2 17.5 2 12A10 10 0 0 1 12 2m.5 5v5.25l4.5 2.67l-.75 1.23L11 13V7z"/></svg> Date: {{ $event->date }}</p>
                    <p class="text-lg text-gray-600 flex gap-2 items-center"> <svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" viewBox="0 0 48 48"><path fill="currentColor" d="M24.005 15.5a6 6 0 1 0 0 12a6 6 0 0 0 0-12m-3.5 6a3.5 3.5 0 1 1 7 0a3.5 3.5 0 0 1-7 0M37 32L26.912 42.71a4 4 0 0 1-5.824 0L11 32h.038l-.017-.02l-.021-.025A16.92 16.92 0 0 1 7 21c0-9.389 7.611-17 17-17s17 7.611 17 17a16.92 16.92 0 0 1-4 10.955l-.021.025l-.017.02zm-1.943-1.619A14.43 14.43 0 0 0 38.5 21c0-8.008-6.492-14.5-14.5-14.5S9.5 12.992 9.5 21a14.43 14.43 0 0 0 3.443 9.381l.308.363l9.657 10.251a1.5 1.5 0 0 0 2.184 0l9.657-10.251z"/></svg> Location: {{ $event->location }}</p>
                    <p class="text-lg text-gray-600 flex gap-2 items-center"> <svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" viewBox="0 0 1024 1024"><path fill="currentColor" d="M512 512a192 192 0 1 0 0-384a192 192 0 0 0 0 384m0 64a256 256 0 1 1 0-512a256 256 0 0 1 0 512m320 320v-96a96 96 0 0 0-96-96H288a96 96 0 0 0-96 96v96a32 32 0 1 1-64 0v-96a160 160 0 0 1 160-160h448a160 160 0 0 1 160 160v96a32 32 0 1 1-64 0"/></svg> Capacity: <strong>{{ $event->capacity }}</strong></p>
                </div>
                <div class="p-4 mt-4 flex gap-4">
                    <button wire:click="attendTheEvent({{ $event->id }})" class="bg-green-700 rounded-full px-8 py-3 text-white font-bold transform transition duration-500 hover:scale-110">Asistir al evento</button>
                    <div class="flex gap-4">
                        <button class="border-2 rounded-full p-4 border-neutral-950 transition ease-in-out hover:border-green-500 hover:bg-green-500 hover:text-white">
                            <svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" viewBox="0 0 24 24"><path fill="currentColor" d="m20.27 16.265l.705-4.08a1.666 1.666 0 0 0-1.64-1.95h-5.182a.833.833 0 0 1-.821-.969l.663-4.045a4.781 4.781 0 0 0-.09-1.973a1.635 1.635 0 0 0-1.093-1.137l-.145-.047a1.346 1.346 0 0 0-.993.068c-.34.164-.588.463-.68.818l-.476 1.834a7.629 7.629 0 0 1-.656 1.679c-.416.777-1.058 1.4-1.725 1.975l-1.44 1.24a1.67 1.67 0 0 0-.572 1.406l.813 9.393A1.666 1.666 0 0 0 8.596 22h4.649c3.481 0 6.452-2.426 7.024-5.735"/><path fill="currentColor" fill-rule="evenodd" d="M2.968 9.485a.75.75 0 0 1 .78.685l.97 11.236a1.237 1.237 0 1 1-2.468.107V10.234a.75.75 0 0 1 .718-.749" clip-rule="evenodd" opacity="0.5"/></svg>
                        </button>
                        <button class="border-2 rounded-full p-4 border-neutral-950 transition ease-in-out hover:border-pink-900 hover:bg-pink-900 hover:text-white">
                            <svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" viewBox="0 0 24 24"><path fill="currentColor" d="m20.27 8.485l.705 4.08a1.666 1.666 0 0 1-1.64 1.95h-5.182a.833.833 0 0 0-.821.969l.663 4.045a4.797 4.797 0 0 1-.09 1.974c-.14.533-.551.962-1.093 1.136l-.145.047a1.35 1.35 0 0 1-.993-.068a1.264 1.264 0 0 1-.68-.818l-.476-1.834a7.628 7.628 0 0 0-.656-1.679c-.416-.777-1.058-1.4-1.725-1.975l-1.44-1.24a1.668 1.668 0 0 1-.572-1.406l.813-9.393A1.666 1.666 0 0 1 8.596 2.75h4.649c3.481 0 6.452 2.426 7.024 5.735"/><path fill="currentColor" fill-rule="evenodd" d="M2.968 15.265a.75.75 0 0 0 .78-.685l.97-11.236a1.237 1.237 0 1 0-2.468-.107v11.279a.75.75 0 0 0 .718.75" clip-rule="evenodd" opacity="0.5"/></svg>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
