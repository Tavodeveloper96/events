<div>

    <div class="xl:container m-auto px-6 py-20 md:px-12 lg:px-20">
        <div class="m-auto text-center lg:w-8/12 xl:w-7/12">
            <h2 class="text-2xl font-bold text-gray-800 dark:text-white md:text-4xl flex items-center justify-center">
                <svg xmlns="http://www.w3.org/2000/svg" width="3em" height="3em" viewBox="0 0 64 64"><path fill="#cfd8dc" d="M5 38V14h38v24c0 2.2-1.8 4-4 4H9c-2.2 0-4-1.8-4-4"/><path fill="#10b981" d="M43 10v6H5v-6c0-2.2 1.8-4 4-4h30c2.2 0 4 1.8 4 4"/><g fill="#10b981"><circle cx="33" cy="10" r="3"/><circle cx="15" cy="10" r="3"/></g><path fill="#b0bec5" d="M33 3c-1.1 0-2 .9-2 2v5c0 1.1.9 2 2 2s2-.9 2-2V5c0-1.1-.9-2-2-2M15 3c-1.1 0-2 .9-2 2v5c0 1.1.9 2 2 2s2-.9 2-2V5c0-1.1-.9-2-2-2"/><path fill="#90a4ae" d="M13 20h4v4h-4zm6 0h4v4h-4zm6 0h4v4h-4zm6 0h4v4h-4zm-18 6h4v4h-4zm6 0h4v4h-4zm6 0h4v4h-4zm6 0h4v4h-4zm-18 6h4v4h-4zm6 0h4v4h-4zm6 0h4v4h-4zm6 0h4v4h-4z"/></svg>
                Registro para este evento.
            </h2>
        </div>
        <div class="m-auto mt-12 items-center justify-center -space-y-4 md:flex md:space-y-0 md:-space-x-4 xl:w-10/12">
            <div class="group relative z-10 -mx-4 md:mx-0 md:w-6/12 lg:w-5/12">
                <div aria-hidden="true"
                    class="absolute top-0 h-full w-full rounded-3xl border border-gray-100 dark:border-gray-700 dark:shadow-none bg-white dark:bg-gray-800 shadow-2xl shadow-gray-600/10 transition duration-500 group-hover:scale-105">
                </div>
                <div class="relative space-y-6 p-8 sm:p-12">
                    <h3 class="text-center text-3xl font-semibold text-gray-700 dark:text-white">{{ $event->name }}</h3>

                    <ul role="list" class="m-auto space-y-4 py-6 text-gray-600 dark:text-gray-300">
                        <li class="space-x-2">
                            <span class="font-semibold text-primary">&check;</span>
                            <span class="text-2xl">{{ $event->description }}</span>
                        </li>
                        <li class="space-x-2 flex items-center gap-1">
                            <svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" viewBox="0 0 24 24"><path fill="currentColor" d="M12 20a8 8 0 0 0 8-8a8 8 0 0 0-8-8a8 8 0 0 0-8 8a8 8 0 0 0 8 8m0-18a10 10 0 0 1 10 10a10 10 0 0 1-10 10C6.47 22 2 17.5 2 12A10 10 0 0 1 12 2m.5 5v5.25l4.5 2.67l-.75 1.23L11 13V7z"/></svg> Date: {{ $event->date }}
                        </li>
                        <li class="space-x-2 flex items-center gap-1">
                            <svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" viewBox="0 0 48 48"><path fill="currentColor" d="M24.005 15.5a6 6 0 1 0 0 12a6 6 0 0 0 0-12m-3.5 6a3.5 3.5 0 1 1 7 0a3.5 3.5 0 0 1-7 0M37 32L26.912 42.71a4 4 0 0 1-5.824 0L11 32h.038l-.017-.02l-.021-.025A16.92 16.92 0 0 1 7 21c0-9.389 7.611-17 17-17s17 7.611 17 17a16.92 16.92 0 0 1-4 10.955l-.021.025l-.017.02zm-1.943-1.619A14.43 14.43 0 0 0 38.5 21c0-8.008-6.492-14.5-14.5-14.5S9.5 12.992 9.5 21a14.43 14.43 0 0 0 3.443 9.381l.308.363l9.657 10.251a1.5 1.5 0 0 0 2.184 0l9.657-10.251z"/></svg> Location: {{ $event->location }}
                        </li>
                        <li class="space-x-2 flex items-center gap-1">
                            <svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" viewBox="0 0 1024 1024"><path fill="currentColor" d="M512 512a192 192 0 1 0 0-384a192 192 0 0 0 0 384m0 64a256 256 0 1 1 0-512a256 256 0 0 1 0 512m320 320v-96a96 96 0 0 0-96-96H288a96 96 0 0 0-96 96v96a32 32 0 1 1-64 0v-96a160 160 0 0 1 160-160h448a160 160 0 0 1 160 160v96a32 32 0 1 1-64 0"/></svg> Capacity: <strong>{{ $event->capacity }}</strong>
                        </li>
                    </ul>
                    <div
                        class="mt-6 flex items-center justify-center space-x-4 text-center text-lg text-gray-600 dark:text-gray-300">
                        <div class="w-6/12">
                            <label class="block font-medium text-sm text-gray-700" for="tickets" value="tickets" />
                            <input type='number' name='tickets' placeholder='tickets' wire:model="tickets" id="tickets"
                                class="w-full rounded-md py-2.5 px-4 mb-2 border text-sm outline-[#f84525]" min="1" max="2"/>

                            @error('tickets')
                                <span class="error">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="mt-6">
                        @if (session()->has('error'))
                            <div class="p-1.5 font-bold text-xs text-center rounded-lg text-white" style="background-color: red;">
                                {{ session('error') }}
                            </div>
                        @endif
                    </div>
                    <button wire:click="registerAttendance"
                        class="bg-green-800 relative flex h-11 w-full items-center justify-center px-6 before:absolute before:inset-0 rounded-full before:bg-primary transform transition duration-500 hover:scale-105">
                        <span class="relative text-base font-semibold text-white dark:text-dark">Registrar asistencia</span>
                    </button>
                </div>
            </div>

            <div class="group relative md:w-6/12 lg:w-7/12">
                <div aria-hidden="true"
                    class="absolute top-0 h-full w-full rounded-3xl border border-gray-100 dark:border-gray-700 dark:shadow-none bg-white dark:bg-gray-800 shadow-2xl shadow-gray-600/10 transition duration-500 group-hover:scale-105">
                </div>
                <div class="relative p-6 pt-16 md:rounded-r-2xl md:p-8 md:pl-12 lg:p-16 lg:pl-20">
                    <img src="{{URL::asset('/images/andicom.webp')}}" alt="andicom" border="0">
                </div>
            </div>
        </div>
    </div>

</div>
