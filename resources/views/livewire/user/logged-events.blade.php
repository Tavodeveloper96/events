<style>
	.barcode {
		left: 50%;
		box-shadow: 1px 0 0 1px, 5px 0 0 1px, 10px 0 0 1px, 11px 0 0 1px, 15px 0 0 1px, 18px 0 0 1px, 22px 0 0 1px, 23px 0 0 1px, 26px 0 0 1px, 30px 0 0 1px, 35px 0 0 1px, 37px 0 0 1px, 41px 0 0 1px, 44px 0 0 1px, 47px 0 0 1px, 51px 0 0 1px, 56px 0 0 1px, 59px 0 0 1px, 64px 0 0 1px, 68px 0 0 1px, 72px 0 0 1px, 74px 0 0 1px, 77px 0 0 1px, 81px 0 0 1px, 85px 0 0 1px, 88px 0 0 1px, 92px 0 0 1px, 95px 0 0 1px, 96px 0 0 1px, 97px 0 0 1px, 101px 0 0 1px, 105px 0 0 1px, 109px 0 0 1px, 110px 0 0 1px, 113px 0 0 1px, 116px 0 0 1px, 120px 0 0 1px, 123px 0 0 1px, 127px 0 0 1px, 130px 0 0 1px, 131px 0 0 1px, 134px 0 0 1px, 135px 0 0 1px, 138px 0 0 1px, 141px 0 0 1px, 144px 0 0 1px, 147px 0 0 1px, 148px 0 0 1px, 151px 0 0 1px, 155px 0 0 1px, 158px 0 0 1px, 162px 0 0 1px, 165px 0 0 1px, 168px 0 0 1px, 173px 0 0 1px, 176px 0 0 1px, 177px 0 0 1px, 180px 0 0 1px;
		display: inline-block;
		transform: translateX(-90px);
	}
</style>
<div>
    <div class="m-4">
        <h2 class="mx-2 font-semibold text-2xl">Evento que asistiras</h2>
    </div>
    <div class="flex flex-row items-center justify-center min-h-96 bg-center bg-cover">
        <div class="opacity-80 inset-0 z-0"></div>
        <div class="flex w-full h-full mx-auto z-10  rounded-3xl">
            @foreach ($events as $event)
                <div class="flex flex-col w-3/12 ">
                    <div class="bg-white relative drop-shadow-2xl rounded-3xl p-4 m-4 absolute">
                        <div class="flex-none sm:flex">
                            <div class=" relative h-32 w-32   sm:mb-0 mb-3 hidden">
                                <img src="https://tailwindcomponents.com/storage/avatars/njkIbPhyZCftc4g9XbMWwVsa7aGVPajYLRXhEeoo.jpg"
                                    alt="aji" class=" w-32 h-32 object-cover rounded-2xl">
                                <a href="#"
                                    class="absolute -right-2 bottom-2   -ml-3  text-white p-1 text-xs bg-green-400 hover:bg-green-500 font-medium tracking-wider rounded-full transition ease-in duration-300">
                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor"
                                        class="h-4 w-4">
                                        <path
                                            d="M13.586 3.586a2 2 0 112.828 2.828l-.793.793-2.828-2.828.793-.793zM11.379 5.793L3 14.172V17h2.828l8.38-8.379-2.83-2.828z">
                                        </path>
                                    </svg>
                                </a>
                            </div>
                            <div class="flex-auto justify-evenly">
                                <div class="flex items-center justify-between">
                                    <div class="flex items-center  my-1">
                                        <span class="mr-3 rounded-full bg-white w-8 h-8">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="2em" height="2em"
                                                viewBox="0 0 24 24">
                                                <path fill="currentColor"
                                                    d="M5 20v-4.043q0-.667.475-1.139q.474-.472 1.14-.472h10.77q.666 0 1.14.475T19 15.96V20zm4.289-7.423q-1.78 0-3.035-1.255Q5 10.068 5 8.29t1.254-3.035T9.29 4h5.423q1.78 0 3.034 1.254T19 8.29t-1.254 3.034t-3.034 1.254zM6 19h12v-3.038q0-.27-.173-.443t-.442-.173H6.615q-.269 0-.442.173T6 15.962zm3.289-7.423h5.422q1.385 0 2.337-.952T18 8.289t-.952-2.337T14.712 5H9.289q-1.385 0-2.337.952T6 8.289t.952 2.336t2.337.952m-.001-2.558q.31 0 .52-.21t.211-.52t-.21-.52t-.52-.211t-.52.21t-.211.52t.21.52t.52.211m5.423 0q.31 0 .52-.21t.211-.52t-.21-.52t-.52-.211t-.52.21t-.211.52t.21.52t.52.211M12 8.29" />
                                            </svg>
                                        </span>
                                        <h2 class="font-medium">Events <strong>Dev</strong></h2>
                                    </div>
                                    <div class="ml-auto text-blue-800">000{{ $event->id }}</div>
                                </div>
                                <div class="border-b border-dashed border-b-2 my-5 ">
                                    <div class="absolute rounded-full w-5 h-5 bg-gray-100 -mt-2 -left-2"></div>
                                    <div class="absolute rounded-full w-5 h-5 bg-gray-100 -mt-2 -right-2"></div>
                                </div>
                                <div class="flex items-center mb-5 text-sm max-w-md">
                                    <ul role="list"
                                        class="m-auto  space-y-4 py-6 text-gray-600 dark:text-gray-300">
                                        <li class="space-x-2">
                                            <span class="font-semibold text-primary">&check;</span>
                                            <span class="text-2xl text-black-50">{{ $event->event->name }}</span>
                                        </li>
                                        <li class="space-x-2 flex items-center gap-1">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em"
                                                viewBox="0 0 24 24">
                                                <path fill="currentColor"
                                                    d="M12 20a8 8 0 0 0 8-8a8 8 0 0 0-8-8a8 8 0 0 0-8 8a8 8 0 0 0 8 8m0-18a10 10 0 0 1 10 10a10 10 0 0 1-10 10C6.47 22 2 17.5 2 12A10 10 0 0 1 12 2m.5 5v5.25l4.5 2.67l-.75 1.23L11 13V7z" />
                                            </svg> Date: {{ $event->event->date }}
                                        </li>
                                        <li class="space-x-2 flex items-center gap-1">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em"
                                                viewBox="0 0 48 48">
                                                <path fill="currentColor"
                                                    d="M24.005 15.5a6 6 0 1 0 0 12a6 6 0 0 0 0-12m-3.5 6a3.5 3.5 0 1 1 7 0a3.5 3.5 0 0 1-7 0M37 32L26.912 42.71a4 4 0 0 1-5.824 0L11 32h.038l-.017-.02l-.021-.025A16.92 16.92 0 0 1 7 21c0-9.389 7.611-17 17-17s17 7.611 17 17a16.92 16.92 0 0 1-4 10.955l-.021.025l-.017.02zm-1.943-1.619A14.43 14.43 0 0 0 38.5 21c0-8.008-6.492-14.5-14.5-14.5S9.5 12.992 9.5 21a14.43 14.43 0 0 0 3.443 9.381l.308.363l9.657 10.251a1.5 1.5 0 0 0 2.184 0l9.657-10.251z" />
                                            </svg> Location: {{ $event->event->location }}
                                        </li>
                                        <li class="space-x-2 flex items-center gap-1">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em"
                                                viewBox="0 0 1024 1024">
                                                <path fill="currentColor"
                                                    d="M512 512a192 192 0 1 0 0-384a192 192 0 0 0 0 384m0 64a256 256 0 1 1 0-512a256 256 0 0 1 0 512m320 320v-96a96 96 0 0 0-96-96H288a96 96 0 0 0-96 96v96a32 32 0 1 1-64 0v-96a160 160 0 0 1 160-160h448a160 160 0 0 1 160 160v96a32 32 0 1 1-64 0" />
                                            </svg> Cantidad de entradas: <span
                                                class="font-bold text-xl">{{ $event->quantity }}</span></strong>
                                        </li>
                                    </ul>
                                </div>
                                <div class="border-b border-dashed border-b-2 my-5">
                                    <div class="absolute rounded-full w-5 h-5 bg-gray-100 -mt-2 -left-2"></div>
                                    <div class="absolute rounded-full w-5 h-5 bg-gray-100 -mt-2 -right-2"></div>
                                </div>
                                <div class="flex flex-col py-5  justify-center text-sm ">
                                    <h6 class="font-bold text-center">Barcode</h6>

                                    <div class="barcode h-14 w-0 inline-block mt-4 relative left-auto"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
            @if ($events->isEmpty())
                <p>No has registrado entradas para ningún evento.</p>
            @endif
        </div>

    </div>
</div>
