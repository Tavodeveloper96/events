@extends('layouts.app')

@section('content')
    <div class="container mx-auto p-4">
        <h1 class="text-2xl font-bold">{{ $event->name }}</h1>
        <p class="text-gray-700">{{ $event->description }}</p>
        <p class="text-gray-600">Date: {{ $event->date }}</p>
        <p class="text-gray-600">Location: {{ $event->location }}</p>
        <p class="text-gray-600">Capacity: {{ $event->capacity }}</p>
        <!-- Agrega más detalles según sea necesario -->
    </div>
@endsection
