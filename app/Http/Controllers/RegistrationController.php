<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Registration;
use App\Models\Event;
use Illuminate\Support\Facades\Auth;

class RegistrationController extends Controller
{
    public function register($id)
    {
        $event = Event::findOrFail($id);
        $user = Auth::user();

        if ($event->registrations()->where('user_id', $user->id)->exists()) {
            return response()->json(['message' => 'Already registered for this event'], 409);
        }

        if ($event->registrations()->count() >= $event->capacity) {
            return response()->json(['message' => 'Event capacity reached'], 409);
        }

        $registration = new Registration();
        $registration->user_id = $user->id;
        $registration->event_id = $event->id;
        $registration->save();

        return response()->json(['message' => 'Successfully registered for the event'], 200);
    }

    public function userEvents()
    {
        $user = Auth::user();
        $events = $user->registrations()->with('event')->get();

        return response()->json($events, 200);
    }
}


