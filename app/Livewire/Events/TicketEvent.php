<?php

namespace App\Livewire\Events;

use Livewire\Component;
use App\Models\Event;
use App\Models\Ticket;
use Illuminate\Support\Facades\Auth;
class TicketEvent extends Component
{
    public $eventId;
    public $tickets;

    protected $rules = [
        'tickets' => 'required|numeric|min:1|max:2',
    ];
    public function mount($id){
        $this->eventId = $id;
        session()->forget('error');
    }
    public function render()
    {
        $event = Event::findOrFail($this->eventId);
        return view('livewire.events.ticket-event', ['event' => $event]);
    }

    public function updatedTickets()
    {
        $this->resetErrorBag('tickets');
    }

    public function registerAttendance()
    {
        session()->forget('error');

        $this->validate();

        $user = Auth::user();

        $existingTicket = Ticket::where('user_id', $user->id)
                                ->where('event_id', $this->eventId)
                                ->first();

        if ($existingTicket) {
            $newQuantity = $existingTicket->quantity + $this->tickets;

            if ($newQuantity > 2) {
                session()->flash('error', 'No puedes registrar más de 2 entradas por usuario.');
                return;
            }

            $existingTicket->quantity = $newQuantity;
            $existingTicket->save();
        } else {
            Ticket::create([
                'user_id' => $user->id,
                'event_id' => $this->eventId,
                'quantity' => $this->tickets,
            ]);
        }
        return redirect()->to("/event/{$this->eventId}");
    }
}
