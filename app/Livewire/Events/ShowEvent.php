<?php

namespace App\Livewire\Events;

use Livewire\Component;
use App\Models\Event;
use Illuminate\Support\Facades\Auth;
use App\Models\Ticket;
class ShowEvent extends Component
{
    public $eventId;
    public $hasRegistered = false;
    public $quantityRegistered = 0;
    public function mount($id){
        $this->eventId = $id;

        if (Auth::check()) {
            $user = Auth::user();
            // Consultar si el usuario ya tiene registro para este evento
            $ticket = Ticket::where('event_id', $id)
                ->where('user_id', $user->id)
                ->first();
            if ($ticket) {
                $this->hasRegistered = true;
                $this->quantityRegistered = $ticket->quantity;
            }
        }
    }
    public function render()
    {
        $event = Event::findOrFail($this->eventId);
        $quantityRegistered = $this->quantityRegistered > 0;
        $hasRegistered = $this->hasRegistered;
        return view('livewire.events.show-event', compact('event', 'quantityRegistered', 'hasRegistered'));
    }

    public function attendTheEvent($id)
    {
        return redirect()->to("/event/ticket/{$id}");
    }
}
