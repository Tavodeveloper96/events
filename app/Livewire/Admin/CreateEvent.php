<?php

namespace App\Livewire\Admin;

use Livewire\Component;
use App\Models\Event;
use Carbon\Carbon;
class CreateEvent extends Component
{
    public $name;
    public $description;
    public $date;
    public $location;
    public $capacity;
    public function render()
    {
        return view('livewire.admin.create-event');
    }

    public function createEvent()
    {
        $this->validate([
            'name' => 'required|string|max:255',
            'description' => 'required|string|max:255',
            'date' => 'required|date',
            'location' => 'required|string|max:255',
            'capacity' => 'required|integer|min:1',
        ]);

        Event::create([
            'name' => $this->name,
            'description' => $this->description,
            'date' => Carbon::parse($this->date)->format('Y-m-d H:i:s'),
            'location' => $this->location,
            'capacity' => $this->capacity,
        ]);

        session()->flash('message', 'Evento creado con éxito.');

        return redirect()->route('admin.events');
    }
}
