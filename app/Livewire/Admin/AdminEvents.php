<?php

namespace App\Livewire\Admin;

use Livewire\Component;
use App\Models\Event;
use Livewire\Attributes\On;
use App\Models\Ticket;
class AdminEvents extends Component
{
    public function render()
    {
        $events = Event::all();
        return view('livewire.admin.admin-events', ['events' => $events]);
    }
    #[On('delete')]
    public function deleteEvent($id)
    {
        Event::findOrFail($id)->delete();

        Ticket::where('event_id', $id)->delete();

        session()->flash('message', 'Evento eliminado con éxito.');

        return redirect()->route('admin.events');
    }
}
