<?php

namespace App\Livewire\Admin;

use Livewire\Component;
use App\Models\Event;
use Livewire\Attributes\Validate;
class EditEvent extends Component
{
    public $eventId;

    #[validate([
        'eventEdit.name' => 'required',
        'eventEdit.description' => 'required',
        'eventEdit.date' => 'required',
        'eventEdit.location' => 'required',
        'eventEdit.capacity' => 'required',

    ])]
    public $eventEdit = [
        'name' => '',
        'description' => '',
        'date' => '',
        'location' => '',
        'capacity' => '',
    ];
    public function mount($eventId)
    {

        $this->eventId = $eventId;

        $event = Event::find($this->eventId);

        $this->eventEdit['name'] = $event->name;
        $this->eventEdit['description'] = $event->description;
        $this->eventEdit['date'] = $event->date;
        $this->eventEdit['location'] = $event->location;
        $this->eventEdit['capacity'] = $event->capacity;
    }
    public function render()
    {
        return view('livewire.admin.edit-event');
    }

    public function updateEvent()
    {
        $this->validate();

        $event = Event::find($this->eventId);

        $event->update([
            'name' => $this->eventEdit['name'],
            'description' =>  $this->eventEdit['description'],
            'date' => $this->eventEdit['date'],
            'location' => $this->eventEdit['location'],
            'capacity' => $this->eventEdit['capacity'],
        ]);

        session()->flash('message', 'Evento ' . $this->eventEdit['name'] . ' actualizado con éxito.');

        return redirect()->route('admin.events');
    }
}
