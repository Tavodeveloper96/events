<?php

namespace App\Livewire\Auth;

use Livewire\Component;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class Login extends Component
{
    public $email;
    public $password;

    protected $rules = [
        'email' => 'required|string|email',
        'password' => 'required|string',
    ];

    public function login()
    {
        session()->forget('error');

        $credentials = $this->validate();

        if (Auth::attempt($credentials)) {
            $user = Auth::user();
            return redirect()->route('events.index')->with('user', $user);
        } else {
            session()->flash('error', 'Invalid login credentials');
        }
    }

    public function logout(Request $request)
    {
        Auth::logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();

        return redirect()->to('/');
    }

    public function updated($propertyName)
    {
        session()->forget('error');
        $this->validateOnly($propertyName);
    }


    public function render()
    {
        return view('livewire.auth.login');
    }
}
