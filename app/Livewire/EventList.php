<?php

namespace App\Livewire;

use Livewire\Component;
use App\Models\Event;
class EventList extends Component
{
    public $events;

    public function mount()
    {

    }

    public function redirectToEvent($id)
    {
        return redirect()->to("/event/{$id}");
    }

    public function attendTheEvent($id)
    {
        return redirect()->to("/event/ticket/{$id}");
    }

    public function render()
    {
        return view('livewire.event-list', ['events' => $this->events]);
    }
}
