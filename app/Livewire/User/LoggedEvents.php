<?php

namespace App\Livewire\User;

use Livewire\Component;
use App\Models\Ticket;
use Illuminate\Support\Facades\Auth;

class LoggedEvents extends Component
{
    public function render()
    {
        $userId = Auth::id();
        $events = Ticket::where('user_id', $userId)->with('event')->get();

        return view('livewire.user.logged-events', ['events' => $events]);
    }
}
