<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Event;
class EventSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Event::create([
            'name' => 'Laravel Workshop',
            'description' => 'An in-depth workshop on Laravel',
            'date' => '2024-06-15',
            'location' => 'New York',
            'capacity' => 100,
        ]);

        Event::create([
            'name' => 'Vue.js Conference',
            'description' => 'A conference for Vue.js enthusiasts',
            'date' => '2024-07-20',
            'location' => 'San Francisco',
            'capacity' => 200,
        ]);

        Event::create([
            'name' => 'Web Development Bootcamp',
            'description' => 'A bootcamp for aspiring web developers',
            'date' => '2024-08-10',
            'location' => 'Los Angeles',
            'capacity' => 150,
        ]);
    }
}
