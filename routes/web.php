<?php

use App\Http\Controllers\EventController;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use App\Livewire\{EventList};
use App\Livewire\Admin\{AdminEvents,EditEvent, CreateEvent};
use App\Livewire\Events\{ShowEvent, TicketEvent};
use App\Livewire\Auth\{Login,Register};
use App\Http\Controllers\AuthController;
use App\Livewire\User\LoggedEvents;
use App\Http\Middleware\AdminMiddleware;

Route::get('/', EventList::class)->name('events.index');
Route::get('/event/{id}', ShowEvent::class)->name('show.index');

Route::get('/register', Register::class)->name('register');
Route::get('/login', Login::class)->name('login');

Route::middleware('auth:sanctum')->group(function () {
    Route::get('/event/ticket/{id}', TicketEvent::class)->name('ticket.index');
    Route::get('/event/tickets/user', LoggedEvents::class)->name('logged.index');
    Route::post('logout', [AuthController::class, 'logout'])->name('logout');
});

Route::middleware(['auth:sanctum', AdminMiddleware::class])->group(function () {
    Route::get('/events', AdminEvents::class)->name('admin.events');
    Route::get('/events/{eventId}/edit', EditEvent::class)->name('admin.events.edit');
    Route::get('/events/create', CreateEvent::class)->name('admin.events.create');
});
